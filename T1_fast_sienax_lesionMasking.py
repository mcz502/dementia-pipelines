import os
import os.path as op
import glob 
import argparse
import json
from shutil import copyfile
from fsl import wrappers

def T1_fast_sienax_lesionsmasking(T1_path,T2_FLAIR_path,FSLDIR):
    T1_orig       = T1_path
    T2_FLAIR_orig = T2_FLAIR_path
    os.chdir(os.path.dirname(T1_orig))

    lm_dir = os.path.join(os.path.dirname(T1_orig),"T1_fast_sienax_lm")
    isExist = os.path.exists(lm_dir)
    if not isExist:
        os.mkdir(lm_dir)
    os.chdir(lm_dir)

    tmp_dir      = os.path.join(os.path.dirname(T1_orig),"tmp_dir")
    T1_fast_dir  = os.path.join(os.path.dirname(T1_orig),"T1_fast")
    T1_first_dir = os.path.join(os.path.dirname(T1_orig),"T1_first")

    T1_tmp_mat_1 = op.join(tmp_dir, 'tmp_mat_1.mat')
    T1_tmp_mat_2 = op.join(tmp_dir, 'tmp_mat_2.mat')
    T1_tmp_mat_3 = op.join(tmp_dir, 'tmp_mat_3.mat')

    MNI               = op.join(FSLDIR,'data/standard/MNI152_T1_1mm.nii.gz')
    MNI_2mm_brain     = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_brain.nii.gz')
    MNI_2mm_skull     = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_skull.nii.gz')
    MNI_2mm_structseg = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_strucseg.nii.gz')
    MNI_2mm_segperiph = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_strucseg_periph.nii.gz')

    report = []

    wrappers.bet(op.join(op.dirname(T1_orig),"T1"), "T1_brain.nii.gz", s=True)
    #os.remove("T1_brain.nii.gz")

    # These 4 lines are equivalent to sienax's pairreg command:
    #     pairreg ${FSLDIR}/data/standard/MNI152_T1_2mm_brain T1_brain
    #             ${FSLDIR}/data/standard/MNI152_T1_2mm_skull T1_brain_skull
    #             T1_to_MNI_linear.mat
    wrappers.flirt(src=op.join(op.dirname(lm_dir),"T1_brain"), ref=MNI_2mm_brain, omat=T1_tmp_mat_1,
                   schedule=op.join(FSLDIR, 'etc', 'flirtsch',
                                    'pairreg1.sch'),
                   interp="trilinear")
    wrappers.flirt(src="T1_brain_skull", ref=MNI_2mm_skull,
                   omat=T1_tmp_mat_2, init=T1_tmp_mat_1, interp="trilinear",
                   schedule=op.join(FSLDIR, 'etc', 'flirtsch',
                                    'pairreg2.sch'))
    wrappers.fixscaleskew(inmat1=T1_tmp_mat_1, inmat2=T1_tmp_mat_2,
                          omat=T1_tmp_mat_3)
    wrappers.flirt(src=op.join(op.dirname(lm_dir),"T1_brain"), ref=MNI_2mm_brain,
                   omat="T1_to_MNI_linear.mat", init=T1_tmp_mat_3,
                   schedule=op.join(FSLDIR, 'etc', 'flirtsch',
                                    'pairreg3.sch'),
                   interp="trilinear")

    matrix = flirt.readFlirt("T1_to_MNI_linear.mat")
    scales = affine.decompose(matrix)[0]

    vscale = float(scales[0]) * float(scales[1]) * float(scales[2])

    wrappers.applyxfm(src=op.join(op.dirname(T1_orig),"T1"), ref=MNI, out="T1_to_MNI_linear",
                      mat="T1_to_MNI_linear.mat", interp="spline")
    wrappers.applyxfm(src="T1_brain_skull", ref=MNI,
                      out="T1_brain_skull_to_MNI_linear",
                      mat="T1_to_MNI_linear.mat", interp="trilinear")
    wrappers.applywarp(src=MNI_2mm_segperiph, ref=op.join(op.dirname(T1_orig),"T1"),
                       w=op.join(op.dirname(T1_orig),"T1_to_MNI_warp_coef_inv"), out="T1_segperiph",
                       rel=True, interp='trilinear')
    wrappers.fslmaths("T1_segperiph").thr(0.5).bin().\
        run("T1_segperiph")

    wrappers.fslmaths(MNI_2mm_structseg).thr(4.5).bin().\
        run("T1_segvent")
    wrappers.applywarp(src="T1_segvent", ref=op.join(op.dirname(T1_orig),"T1"),
                       w=op.join(op.dirname(T1_orig),"T1_to_MNI_warp_coef_inv"), out="T1_segvent",
                       rel=True, interp='nn')

    lm = op.join(os.path.dirname(T2_FLAIR_orig),"bianca_final_mask.nii.gz")
    wrappers.fslmaths(lm).bin().mul(-1).add(1).mul(op.join(op.dirname(T1_orig),"T1_brain"))
    wrappers.fast(op.join(op.dirname(T1_orig),"T1_brain.nii.gz"),out=op.join(lm_dir, "T1_brain"),g=True)

    wrappers.fslmaths(lm).bin().max(op.join(lm_dir,"T1_brain_pve_2")).run(op.join(lm_dir,"T1_brain_pve_2"))
    wrappers.fslmaths(lm).bin().mul(3).max(op.join(lm_dir,"T1_brain_seg")).run(op.join(lm_dir,"T1_brain_seg"))

    wrappers.fslmaths(op.join(lm_dir,"T1_brain_pve_1")).mas(op.join(lm_dir,"T1_segperiph")).run(op.join(lm_dir,"T1_pve_1_segperiph"),odt="float")

    V   = wrappers.fslstats(op.join(lm_dir,"T1_pve_1_segperiph")).m.v.run()
    xa  = float(V[0])
    xb  = float(V[2])
    uxg = xa * xb
    xg  = xa * xb * vscale

    report.append(f'VSCALING {vscale}')
    report.append('tissue             volume    unnormalised-volume')
    report.append(f'pgrey              {xg} {uxg} (peripheral grey)')

    #wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_0")).mas("T1_segvent").\
    #    run("T1_pve_0_segperiph", odt="float")
    wrappers.fslmaths(op.join(lm_dir,"T1_brain_pve_0")).mas("T1_segvent").run(op.join(lm_dir,"T1_pve_0_segperiph"),odt="float")

    V   = wrappers.fslstats(op.join(lm_dir,"T1_pve_0_segperiph")).m.v.run()
    xa  = float(V[0])
    xb  = float(V[2])
    uxg = xa * xb
    xg  = xa * xb * vscale
    report.append(f'vcsf               {xg} {uxg} (ventricular CSF)')

    V     = wrappers.fslstats(op.join(lm_dir,"T1_brain_pve_1")).m.v.run()
    xa    = float(V[0])
    xb    = float(V[2])
    ugrey = xa * xb
    ngrey = xa * xb * vscale
    report.append(f'GREY               {ngrey} {ugrey}')

    V      = wrappers.fslstats(op.join(lm_dir,"T1_brain_pve_2")).m.v.run()
    xa     = float(V[0])
    xb     = float(V[2])
    uwhite = xa * xb
    nwhite = xa * xb * vscale
    report.append(f'WHITE              {nwhite} {uwhite}')

    ubrain = ugrey + uwhite
    nbrain = ngrey + nwhite
    report.append(f'BRAIN              {nbrain} {ubrain}')

    with open(op.join(lm_dir,"report.sienax"), 'wt', encoding="utf-8") as f:
        for line in report:
            f.write(f'{line}\n')
            
def IDP_json_generation(T1_path,idp_data):
    
    workdir = op.join(op.dirname(T1_path),"IDPs")
    os.chdir(workdir)    
    # Get FIRST volumes normalised
    IDPs_json_file = op.join(idp_data,"IDPs.json")
    with open(IDPs_json_file, "r", encoding="utf-8") as f:
        IDPs_dict = json.load(f)    
    sienax_idp = IDPs_dict["IDP_T1_SIENAX"]
    with open(op.join(workdir,"IDP_T1_SIENAX.txt")) as f:
        tmp      = f.read()
        tmp_vals = tmp.split()
        nums     = [float(num) for num in tmp_vals]
        for j in range(len(sienax_idp)):
            if sienax_idp[j]['name'] == "T1_SIENAX_headsize_scaling":
                idp_pos = sienax_idp[j]['position']
                idp_val = nums[idp_pos-1]
    with open(op.join(workdir,"IDP_T1_FIRST_vols.txt")) as f:
        tmp        = f.read()
        tmp_vals   = tmp.split()
    constant = idp_val  # Replace with your desired constant
    values = [int(value) for value in tmp_vals]  # Convert the values to integers
    modified_values = [value * constant for value in values]
    # Save the modified values to another text file
    output_file = op.join(workdir,"IDP_T1_FIRST_vols_norm.txt")
    with open(output_file, "w") as file:
        for value in modified_values:
            file.write(str(value) + " ")
    # Get final IDPs in json
    idp_files = ["IDP_T1_SIENAX","biancaMasked_IDP_T1_SIENAX","IDP_T1_FIRST_vols_norm"]
    idp_list  = ["T1_SIENAX_headsize_scaling", "T1_SIENAX_brain-norm_vol", "T1_SIENAX_GM_norm_vol",
                "T1_SIENAX_WM_norm_vol", "T1_SIENAX_CSF_norm_vol", "biancaMasked_T1_SIENAX_brain-norm_vol", "masked_T1_SIENAX_GM_norm_vol",
                "biancaMasked_T1_SIENAX_WM_norm_vol", "biancaMasked_T1_SIENAX_CSF_norm_vol", "T1_FIRST_left_hippocampus",
                "T1_FIRST_right_hippocampus"]
    json_obj_list = []
    for file in idp_files:
        idp_names = IDPs_dict[file]
        with open(op.join(workdir,file + ".txt")) as f:
            temp     = f.read()
            vals_str = temp.split()
            numbers  = [float(num) for num in vals_str]              
        for i in range(len(IDPs_dict[file])):
            for key in idp_list:
                if key == idp_names[i]['name']:
                    pos = idp_names[i]['position']
                    json_obj_list.append({key: numbers[pos-1]})
    with open(op.join(workdir,'IDPs_Report_withLesionMasking.json'), 'w') as f:
        json.dump(json_obj_list, f, indent=4)
        
def main():
    parser = argparse.ArgumentParser(description='--Re-run T1w FAST and SIENAX with lesion masking (make sure you finished main_t1.py and main_flair.py before running this script --')
    parser.add_argument("T1_path",       help = 'path to T1w with filename')
    parser.add_argument("T2_FLAIR_path", help = 'path to GDC with filename')
    parser.add_argument("fsl_dir",       help = 'path to FSL')
    parser.add_argument("IDP_data",      help = 'path to IDP data')

    args            = parser.parse_args()   
    T1_path         = args.T1_path    
    T2_FLAIR_path   = args.T2_FLAIR_path
    FSLDIR          = args.fsl_dir
    IDP_data        = args.IDP_data
    
    print('\n ---- Running lesion masking : %s ---- \n'% os.path.basename(T1_path))
    T1_fast_sienax_lesionsmasking(T1_path,T2_FLAIR_path,FSLDIR)    
    print('\n **** Completed lesion masking : %s **** \n'% os.path.basename(T1_path))
    
    print('\n ---- Running IDP json creation : %s ---- \n'% os.path.basename(T1_path))
    IDP_json_generation(T1_path,IDP_data)
    print('\n ---- Completed IDP json creation : %s ---- \n'% os.path.basename(T1_path))
    
    print('\n **** Finished! : %s **** \n'% os.path.basename(T1_path))
    print('\n **** Required IDPs are stored in %s **** \n'% op.join(op.dirname(T1_path),"IDPs/IDPs_Report.json"))
    
if __name__ == "__main__":
    main()