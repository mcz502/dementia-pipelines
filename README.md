# Dementia pipelines


## --Run T1w processing (Biobank style)--

### Usage: main_t1.py [-h] T1_path gdc_file fsl_dir fnirt_data_dir templ idp_data

Positional arguments: \
  T1_path:         path to T1w with filename \
  gdc_file:        path to GDC with filename \
  fsl_dir:         path to FSL \
  fnirt_data_dir:  path to fnirt folder \
  templ:           path to templates folder \
  idp_data:        path to IDP ref folder 

## --Run T2 FLAIR processing (Biobank style)--
  
### Usage: main_flair.py [-h] T2_FLAIR_path gdc_file T1_dir fsl_dir bianca_data templ

Positional arguments: \
  T2_FLAIR_path:  path to T1w with filename \
  gdc_file:       path to GDC with filename \
  T1_dir:         path to T1 dir \
  fsl_dir:        path to FSL \
  bianca_data:    path to bianca data \
  templ:          path to templates folder 

## --Re-run T1w FAST and SIENAX with lesion masking (make sure you finished main_t1.py and main_flair.py before running this script --

### Usage: T1_fast_sienax_lesionMasking.py [-h] T1_path T2_FLAIR_path fsl_dir IDP_data

Positional arguments: \
  T1_path:        path to T1w with filename \
  T2_FLAIR_path:  path to GDC with filename \
  fsl_dir:        path to FSL \
  IDP_data:       path to IDP data 
