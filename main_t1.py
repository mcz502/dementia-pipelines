import os
import os.path as op
import glob 
import argparse
import json
from shutil import copyfile
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from fsl import wrappers
from fsl.transform import affine, flirt


def T1_gdc(T1_path,gdc_file,FSLDIR,fnirt_data_dir,templ):
    
    T1_orig = T1_path
    gdc     = gdc_file 
    os.chdir(os.path.dirname(T1_orig))

    T1_GDC          = os.path.join(os.path.dirname(T1_orig),'T1_GDC')
    T1_orig_ud      = "T1_orig_ud.nii.gz"
    T1_orig_ud_warp = "T1_orig_ud_warp.nii.gz"

    if gdc not in (None, "", "none"):
        # Calculate and apply the Gradient Distortion Unwarp
        # TODO: Review the "half=True" in next version
        gradient_unwarp_apply(WD=T1_GDC, infile=T1_orig, outfile=T1_orig_ud,
                              owarp=T1_orig_ud_warp, gradcoeff=gdc,
                              vendor='siemens', nojac=True, half=True)
    else:
        copyfile(src=T1_orig, dst=T1_orig_ud)

def T1_brain_extract(T1_path,gdc_file,FSLDIR,fnirt_data_dir,templ):
    
    T1_orig = T1_path
    gdc     = gdc_file
    os.chdir(os.path.dirname(T1_orig))
    tmp_dir= os.path.join(os.path.dirname(T1_orig),"tmp_dir")

    isExist = os.path.exists(tmp_dir)

    if not isExist:
        os.mkdir(tmp_dir)

    T1_tmp_1= op.join(tmp_dir, 'T1_tmp_1.nii.gz')
    T1_tmp_2= op.join(tmp_dir, 'T1_tmp_2.nii.gz')
    T1_tmp_1_brain= op.join(tmp_dir, 'T1_tmp_1_brain.nii.gz')
    T1_tmp_orig_ud_to_std_mat= op.join(tmp_dir, 'T1_tmp_to_std.mat')
    T1_tmp= op.join(tmp_dir, 'T1.nii.gz')
    T1_tmp_prefix= op.join(tmp_dir, 'T1')

    head_top= int(round(float(wrappers.robustfov(os.path.join(os.path.dirname(T1_orig),"T1_orig_ud")).stdout[0]. split()[7])))
    wrappers.fslmaths(os.path.join(os.path.dirname(T1_orig),"T1_orig_ud")).roi(0, -1, 0, -1, head_top, 170, 0, 1, ).\
       run(T1_tmp_1)

    # Run a (Recursive) brain extraction on the roi
    wrappers.bet(T1_tmp_1, T1_tmp_1_brain, robust=True)

    # Reduce the FOV of T1_orig_ud by calculating a registration
    # from T1_tmp_brain to ssref and apply it to T1_orig_ud
    wrappers.standard_space_roi(T1_tmp_1_brain, T1_tmp_prefix,
                                maskNONE=True,
                                ssref=os.path.join(FSLDIR,"data/standard/MNI152_T1_1mm_brain"),
                                altinput=os.path.join(os.path.dirname(T1_orig),"T1_orig_ud"), d=True)

    os.chdir(os.path.dirname(T1_orig))
    copyfile(src=T1_tmp, dst="T1.nii.gz")
    copyfile(src=os.path.join(tmp_dir,"T1_tmp_to_std.mat"), dst="T1_orig_ud_to_std.mat") # Fidel's code v2.0 has different argument which doesn't exist

    # Generate the actual affine from the orig_ud volume to the cut version
    # we haveand combine it to have an affine matrix from orig_ud to MNI
    wrappers.flirt(src="T1", ref=os.path.join(os.path.dirname(T1_orig),"T1_orig_ud"), omat="T1_to_T1_orig_ud.mat",       schedule=os.path.join(FSLDIR,"etc/flirtsch/xyztrans.sch"))

    wrappers.invxfm(inmat="T1_to_T1_orig_ud.mat", omat="T1_orig_ud_to_T1.mat")
    wrappers.concatxfm(atob="T1_to_T1_orig_ud.mat", btoc="T1_orig_ud_to_std.mat", atoc="T1_to_MNI_linear.mat")
    wrappers.fnirt(src="T1", ref=os.path.join(FSLDIR,"data/standard/MNI152_T1_1mm"), aff="T1_to_MNI_linear.mat", config=os.path.join(fnirt_data_dir,"bb_fnirt.cnf"), refmask=os.path.join(templ,"MNI152_T1_1mm_brain_mask_dil_GD7"), logout="bb_T1_to_MNI_fnirt.log", cout="T1_to_MNI_warp_coef", fout="T1_to_MNI_warp",jout="T1_to_MNI_warp_jac",iout=T1_tmp_2, interp='spline')
    # Nonlinear registration to MNI using
    # the previously calculated alignment

    # Combine all transforms (Gradient Distortion Unwarp and T1 to ctx.MNI)
    if gdc not in (None, "", "none"):
        wrappers.convertwarp(ref=os.path.join(FSLDIR,"data/standard/MNI152_T1_1mm"), warp1=os.path.join(os.path.dirname(T1_orig),"T1_orig_ud_warp"),midmat="T1_orig_ud_to_T1.mat",warp2="T1_to_MNI_warp",out="T1_orig_to_MNI_warp")
    else:
        wrappers.convertwarp(ref=os.path.join(FSLDIR,"data/standard/MNI152_T1_1mm"), premat="T1_orig_ud_to_T1.mat", warp1=os.path.join(os.path.dirname(T1_orig),"T1_to_MNI_warp"), out="T1_orig_to_MNI_warp") # issue with Fidel's v2.0 referred to v1.5 to provide T1_to_MNI_warp since we are doing GDC

    # Apply the previously generated transformation
    wrappers.applywarp(src=T1_orig, ref=os.path.join(FSLDIR,"data/standard/MNI152_T1_1mm"), w="T1_orig_to_MNI_warp", out="T1_brain_to_MNI", rel=True, interp='spline')

    # Create brain mask
    wrappers.invwarp(ref="T1", warp="T1_to_MNI_warp_coef", out="T1_to_MNI_warp_coef_inv")
    MNI_var_name = os.path.join(templ,"MNI152_T1_1mm_brain_mask.nii.gz")
    wrappers.applywarp(src=MNI_var_name, ref="T1", w="T1_to_MNI_warp_coef_inv", out="T1_brain_mask", rel=True, interp='trilinear')
    wrappers.fslmaths("T1").mul("T1_brain_mask").run("T1_brain")
    wrappers.fslmaths("T1_brain_to_MNI").mul(MNI_var_name).run("T1_brain_to_MNI")

def T1_defacing(T1_path,FSLDIR,fnirt_data_dir,templ):
    
    T1_orig = T1_path
    os.chdir(os.path.dirname(T1_orig))
    tmp_dir= os.path.join(os.path.dirname(T1_orig),"tmp_dir")

    T1_tmp_4                  = op.join(tmp_dir, 'T1_tmp_4.nii.gz')
    T1_tmp_mat                = op.join(tmp_dir, 'T1_tmp.mat')

    # TODO: Replace this part with proper call to fsl_deface
    # Defacing T1_orig
    wrappers.concatxfm(atob="T1_orig_ud_to_T1.mat", btoc="T1_to_MNI_linear.mat",
                       atoc=T1_tmp_mat)
    wrappers.concatxfm(atob=T1_tmp_mat,
                       btoc=os.path.join(templ,"MNI_to_MNI_BigFoV_facemask.mat"),
                       atoc=T1_tmp_mat)
    wrappers.invxfm(inmat=T1_tmp_mat, omat=T1_tmp_mat)
    wrappers.applyxfm(src=os.path.join(templ,"MNI152_T1_1mm_BigFoV_facemask"),
                      ref=T1_orig, mat=T1_tmp_mat, out="T1_defacing_mask",
                      interp="trilinear")
    wrappers.fslmaths("T1_defacing_mask").binv().mul(T1_orig).\
        run("T1_orig_defaced")

    # Defacing T1
    wrappers.concatxfm(atob="T1_to_MNI_linear.mat",
                       btoc=os.path.join(templ,"MNI_to_MNI_BigFoV_facemask.mat"),
                       atoc=T1_tmp_mat)
    wrappers.invxfm(inmat=T1_tmp_mat, omat=T1_tmp_mat)
    wrappers.applyxfm(src=os.path.join(templ,"MNI152_T1_1mm_BigFoV_facemask"),
                      ref="T1", mat=T1_tmp_mat, out="T1_defacing_mask",
                      interp="trilinear")
    wrappers.fslmaths("T1_defacing_mask").binv().mul("T1").run("T1")

    # Generation of QC value: Number of voxels in which
    # the defacing mask goes into the brain mask
    wrappers.fslmaths("T1_brain_mask").thr(0.5).bin().run(T1_tmp_4)
    wrappers.fslmaths("T1_defacing_mask").thr(0.5).bin().add(T1_tmp_4).\
        run(T1_tmp_4)
    vals = wrappers.fslstats(T1_tmp_4).V.run()[0]
    with open("T1_QC_face_mask_inside_brain_mask.txt", 'wt', encoding="utf-8") as f:
        f.write(str(vals))

def T1_fast(T1_path,FSLDIR,fnirt_data_dir,templ):
    
    T1_orig = T1_path
    os.chdir(os.path.dirname(T1_orig))
    
    T1_fast_dir=os.path.join(os.path.dirname(T1_orig),"T1_fast")

    isExist = os.path.exists(T1_fast_dir)

    if not isExist:
        os.mkdir(T1_fast_dir)

    wrappers.fast("T1_brain.nii.gz", out=op.join(T1_fast_dir, "T1_brain"), b=True)

    # Binarize PVE masks
    wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_0")).thr(0.5).bin().run(op.join(T1_fast_dir,"T1_brain_CSF_mask"))
    wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_1")).thr(0.5).bin().run(op.join(T1_fast_dir,"T1_brain_GM_mask"))
    wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_2")).thr(0.5).bin().run(op.join(T1_fast_dir,"T1_brain_WM_mask"))

    # Apply bias field correction to T1
    wrappers.fslmaths("T1").div(op.join(T1_fast_dir,"T1_brain_bias")).run("T1_unbiased")
    wrappers.fslmaths("T1_brain").div(op.join(T1_fast_dir,"T1_brain_bias")).run("T1_unbiased_brain")

def T1_first(T1_path,FSLDIR,fnirt_data_dir,templ):
    
    T1_orig = T1_path
    os.chdir(os.path.dirname(T1_orig))
    
    T1_first_dir=os.path.join(os.path.dirname(T1_orig),"T1_first")

    isExist = op.exists(T1_first_dir)

    if not isExist:
        os.mkdir(T1_first_dir)

    os.chdir(T1_first_dir)    

    # Creates a link inside T1_first to T1_unbiased_brain.nii.gz
    if os.path.join(op.dirname(T1_orig),"T1_unbiased_brain.nii.gz") is not None and not op.exists(op.join(op.dirname(T1_orig),"T1_unbiased_brain.nii.gz")):
        rel_path = op.relpath(op.join(op.dirname(T1_orig),"T1_unbiased_brain.nii.gz")),op.dirname(op.join(op.dirname(T1_orig),"T1_unbiased_brain.nii.gz"))
        os.symlink(src=rel_path, dst=op.join(op.dirname(T1_orig),"T1_first_unbiased_brain.nii.gz"))

    wrappers.run_first_all(input=op.join(op.dirname(T1_orig),"T1_unbiased_brain.nii.gz"), output="T1_first", b=True)

    for f in glob.glob("T1_first" + "-*_first*.nii.gz"):
        if op.exists(f):
            os.remove(f)
    for f in glob.glob("T1_first" + "-*_corr*.nii.gz"):
        if op.exists(f):
            os.remove(f)

def T1_sienax(T1_path,FSLDIR,fnirt_data_dir,templ):
    
    T1_orig = T1_path
    os.chdir(os.path.dirname(T1_orig))
    tmp_dir= os.path.join(os.path.dirname(T1_orig),"tmp_dir")
    T1_fast_dir=os.path.join(os.path.dirname(T1_orig),"T1_fast")
    T1_first_dir=os.path.join(os.path.dirname(T1_orig),"T1_first")
    T1_sienax_dir=os.path.join(os.path.dirname(T1_orig),"T1_sienax")

    isExist = op.exists(T1_sienax_dir)

    if not isExist:
        os.mkdir(T1_sienax_dir)

    os.chdir(T1_sienax_dir)  

    T1_tmp_mat_1 = op.join(tmp_dir, 'tmp_mat_1.mat')
    T1_tmp_mat_2 = op.join(tmp_dir, 'tmp_mat_2.mat')
    T1_tmp_mat_3 = op.join(tmp_dir, 'tmp_mat_3.mat')

    MNI               = op.join(FSLDIR,'data/standard/MNI152_T1_1mm.nii.gz')
    MNI_2mm_brain     = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_brain.nii.gz')
    MNI_2mm_skull     = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_skull.nii.gz')
    MNI_2mm_structseg = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_strucseg.nii.gz')
    MNI_2mm_segperiph = op.join(FSLDIR,'data/standard/MNI152_T1_2mm_strucseg_periph.nii.gz')

    report = []

    wrappers.bet(op.join(op.dirname(T1_orig),"T1"), " T1_brain.nii.gz", s=True)
    os.remove("T1_brain.nii.gz")

    # These 4 lines are equivalent to sienax's pairreg command:
    #     pairreg ${FSLDIR}/data/standard/MNI152_T1_2mm_brain T1_brain
    #             ${FSLDIR}/data/standard/MNI152_T1_2mm_skull T1_brain_skull
    #             T1_to_MNI_linear.mat
    wrappers.flirt(src=op.join(op.dirname(T1_orig),"T1_brain"), ref=MNI_2mm_brain, omat=T1_tmp_mat_1,
                   schedule=op.join(FSLDIR, 'etc', 'flirtsch',
                                    'pairreg1.sch'),
                   interp="trilinear")
    wrappers.flirt(src="T1_brain_skull", ref=MNI_2mm_skull,
                   omat=T1_tmp_mat_2, init=T1_tmp_mat_1, interp="trilinear",
                   schedule=op.join(FSLDIR, 'etc', 'flirtsch',
                                    'pairreg2.sch'))
    wrappers.fixscaleskew(inmat1=T1_tmp_mat_1, inmat2=T1_tmp_mat_2,
                          omat=T1_tmp_mat_3)
    wrappers.flirt(src=op.join(op.dirname(T1_orig),"T1_brain"), ref=MNI_2mm_brain,
                   omat="T1_to_MNI_linear.mat", init=T1_tmp_mat_3,
                   schedule=op.join(FSLDIR, 'etc', 'flirtsch',
                                    'pairreg3.sch'),
                   interp="trilinear")

    matrix = flirt.readFlirt("T1_to_MNI_linear.mat")
    scales = affine.decompose(matrix)[0]

    vscale = float(scales[0]) * float(scales[1]) * float(scales[2])

    wrappers.applyxfm(src=op.join(op.dirname(T1_orig),"T1"), ref=MNI, out="T1_to_MNI_linear",
                      mat="T1_to_MNI_linear.mat", interp="spline")
    wrappers.applyxfm(src="T1_brain_skull", ref=MNI,
                      out="T1_brain_skull_to_MNI_linear",
                      mat="T1_to_MNI_linear.mat", interp="trilinear")
    wrappers.applywarp(src=MNI_2mm_segperiph, ref=op.join(op.dirname(T1_orig),"T1"),
                       w=op.join(op.dirname(T1_orig),"T1_to_MNI_warp_coef_inv"), out="T1_segperiph",
                       rel=True, interp='trilinear')
    wrappers.fslmaths("T1_segperiph").thr(0.5).bin().\
        run("T1_segperiph")

    wrappers.fslmaths(MNI_2mm_structseg).thr(4.5).bin().\
        run("T1_segvent")
    wrappers.applywarp(src="T1_segvent", ref=op.join(op.dirname(T1_orig),"T1"),
                       w=op.join(op.dirname(T1_orig),"T1_to_MNI_warp_coef_inv"), out="T1_segvent",
                       rel=True, interp='nn')
    #wrappers.fslmaths(op.join(T1_fast_dir,"T1_fast_pve_1")).mas("T1_segperiph").\
    #    run("T1_pve_1_segperiph", odt="float")
    wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_1")).mas("T1_segperiph").run("T1_pve_1_segperiph",odt="float")

    V   = wrappers.fslstats("T1_pve_1_segperiph").m.v.run()
    xa  = float(V[0])
    xb  = float(V[2])
    uxg = xa * xb
    xg  = xa * xb * vscale

    report.append(f'VSCALING {vscale}')
    report.append('tissue             volume    unnormalised-volume')
    report.append(f'pgrey              {xg} {uxg} (peripheral grey)')

    #wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_0")).mas("T1_segvent").\
    #    run("T1_pve_0_segperiph", odt="float")
    wrappers.fslmaths(op.join(T1_fast_dir,"T1_brain_pve_0")).mas("T1_segvent").run("T1_pve_0_segperiph",odt="float")

    V   = wrappers.fslstats("T1_pve_0_segperiph").m.v.run()
    xa  = float(V[0])
    xb  = float(V[2])
    uxg = xa * xb
    xg  = xa * xb * vscale
    report.append(f'vcsf               {xg} {uxg} (ventricular CSF)')

    V     = wrappers.fslstats(op.join(T1_fast_dir,"T1_brain_pve_1")).m.v.run()
    xa    = float(V[0])
    xb    = float(V[2])
    ugrey = xa * xb
    ngrey = xa * xb * vscale
    report.append(f'GREY               {ngrey} {ugrey}')

    V      = wrappers.fslstats(op.join(T1_fast_dir,"T1_brain_pve_2")).m.v.run()
    xa     = float(V[0])
    xb     = float(V[2])
    uwhite = xa * xb
    nwhite = xa * xb * vscale
    report.append(f'WHITE              {nwhite} {uwhite}')

    ubrain = ugrey + uwhite
    nbrain = ngrey + nwhite
    report.append(f'BRAIN              {nbrain} {ubrain}')

    with open("report.sienax", 'wt', encoding="utf-8") as f:
        for line in report:
            f.write(f'{line}\n')
            
def T1_idps(T1_path,IDP_data):
    T1_orig  = T1_path
    
    IDPs    = op.join(op.dirname(T1_orig),"IDPs")
    isExist = op.exists(IDPs)

    if not isExist:
        os.mkdir(IDPs)
    
    # FIRST IDPs 
    os.chdir(op.dirname(T1_orig))
    if op.join(os.path.dirname(T1_orig),"T1_first","T1_first_all_fast_firstseg.nii.gz") is not None and op.exists(op.join(os.path.dirname(T1_orig),"T1_first","T1_first_all_fast_firstseg.nii.gz")):
        v = wrappers.fslstats(op.join(os.path.dirname(T1_orig),"T1_first","T1_first_all_fast_firstseg.nii.gz")).H(58, 0.5, 58.5).run()
        # Check that the outputs are OK
        if len(v) == 58:
            # indices that we are interested in
            ind = [9, 48, 10, 49, 11, 50, 12, 51, 16, 52, 17, 53, 25, 57,15]
            result = [str(int(v[x])) for x in ind]
            result = " ".join(result)
    with open(op.join(IDPs,"IDP_T1_FIRST_vols.txt"), 'wt', encoding="utf-8") as f:
        f.write(f'{result}\n')
        
    # SIENAX IDPs 
    os.chdir(op.dirname(T1_orig))
    result = ("NaN " * 11).strip()
    if op.join(os.path.dirname(T1_orig),"T1_sienax","report.sienax") is not None and op.join(os.path.dirname(T1_orig),"T1_sienax","report.sienax"):
        with open(op.join(os.path.dirname(T1_orig),"T1_sienax","report.sienax"), "r",  encoding="utf-8") as f:
            text = f.readlines()
            # indices that we are interested in
            result  = text[0].split()[1] + " "
            result += text[2].split()[1] + " " + text[2].split()[2] + " "
            result += text[3].split()[1] + " " + text[3].split()[2] + " "
            result += text[4].split()[1] + " " + text[4].split()[2] + " "
            result += text[5].split()[1] + " " + text[5].split()[2] + " "
            result += text[6].split()[1] + " " + text[6].split()[2]
    with open(op.join(IDPs,"IDP_T1_SIENAX.txt"), 'wt', encoding="utf-8") as f:
        f.write(f'{result}\n')
    copyfile(src=op.join(IDPs,"IDP_T1_SIENAX.txt"), dst=op.join(IDPs,"T1_sienax.txt"))
    
    # GM parcellation
    os.chdir(op.dirname(T1_orig))
    tmp_dir= os.path.join(os.path.dirname(T1_orig),"tmp_dir")
    isExist = os.path.exists(tmp_dir)
    if not isExist:
        os.mkdir(tmp_dir)
    GMatlas_to_T1 = op.join(tmp_dir, 'GMatlas_to_T1.nii.gz')
    GMatlas = op.join(IDP_data, "GMatlas/GMatlas.nii.gz")
    wrappers.applywarp(src=GMatlas, ref="T1", w="T1_to_MNI_warp_coef_inv",
                       out=GMatlas_to_T1, interp='nn')
    vals = wrappers.fslstats(op.join(os.path.dirname(T1_orig),"T1_fast","T1_brain_pve_1"), K=GMatlas_to_T1).m.v.run()
    result = " ".join([str(x) for x in vals[:, 0] * vals[:, 1]])
    with open(op.join(IDPs,"IDP_T1_GM_parcellation.txt"), 'wt', encoding="utf-8") as f:
        f.write(f'{result}\n')
        
    # IDP generators for all: this section is working
    os.chdir(op.join(op.dirname(T1_orig),"IDPs"))
    result = ""
    IDPs_json_file = op.join(IDP_data,"IDPs.json")
    with open(IDPs_json_file, "r", encoding="utf-8") as f:
        IDPs_dict = json.load(f)
    for IDP_file in ["IDP_T1_SIENAX.txt",
                     "IDP_T1_FIRST_vols.txt", "IDP_T1_GM_parcellation.txt"]:
        plain_name = op.basename(IDP_file).replace(".txt", "")
        num_IDPs = len(IDPs_dict[plain_name])
        result_nans = ("NaN " * num_IDPs).strip()
        if IDP_file is not None and op.exists(IDP_file):
            with open(IDP_file, "r", encoding="utf-8") as f:
                IDPs_l = f.read().strip().split()
            if len(IDPs_l) != num_IDPs:
                result += " " + result_nans
            else:
                result += " " + " ".join(IDPs_l)
        else:
            result += " " + result_nans
    result = result.replace("  ", " ").strip()
    print(result)
    with open("IDPs.txt", 'wt', encoding="utf-8") as f:
        f.write(f'{result}\n')
        
def IDP_json_generation(T1_path,idp_data):
    
    workdir = op.join(op.dirname(T1_path),"IDPs")
    os.chdir(workdir)    
    # Get FIRST volumes normalised
    IDPs_json_file = op.join(idp_data,"IDPs.json")
    with open(IDPs_json_file, "r", encoding="utf-8") as f:
        IDPs_dict = json.load(f)    
    sienax_idp = IDPs_dict["IDP_T1_SIENAX"]
    with open(op.join(workdir,"IDP_T1_SIENAX.txt")) as f:
        tmp      = f.read()
        tmp_vals = tmp.split()
        nums     = [float(num) for num in tmp_vals]
        for j in range(len(sienax_idp)):
            if sienax_idp[j]['name'] == "T1_SIENAX_headsize_scaling":
                idp_pos = sienax_idp[j]['position']
                idp_val = nums[idp_pos-1]
    with open(op.join(workdir,"IDP_T1_FIRST_vols.txt")) as f:
        tmp        = f.read()
        tmp_vals   = tmp.split()
    constant = idp_val  # Replace with your desired constant
    values = [int(value) for value in tmp_vals]  # Convert the values to integers
    modified_values = [value * constant for value in values]
    # Save the modified values to another text file
    output_file = op.join(workdir,"IDP_T1_FIRST_vols_norm.txt")
    with open(output_file, "w") as file:
        for value in modified_values:
            file.write(str(value) + " ")
    # Get final IDPs in json
    idp_files = ["IDP_T1_SIENAX","IDP_T1_FIRST_vols_norm"]
    idp_list  = ["T1_SIENAX_headsize_scaling", "T1_SIENAX_brain-norm_vol", "T1_SIENAX_GM_norm_vol",
                "T1_SIENAX_WM_norm_vol", "T1_SIENAX_CSF_norm_vol", "T1_FIRST_left_hippocampus",
                "T1_FIRST_right_hippocampus"]
    json_obj_list = []
    for file in idp_files:
        idp_names = IDPs_dict[file]
        with open(op.join(workdir,file + ".txt")) as f:
            temp     = f.read()
            vals_str = temp.split()
            numbers  = [float(num) for num in vals_str]              
        for i in range(len(IDPs_dict[file])):
            for key in idp_list:
                if key == idp_names[i]['name']:
                    pos = idp_names[i]['position']
                    json_obj_list.append({key: numbers[pos-1]})
    with open(op.join(workdir,'IDPs_Report.json'), 'w') as f:
        json.dump(json_obj_list, f, indent=4)
        
def main():
    parser = argparse.ArgumentParser(description='--Run T1w processing (Biobank style)--')
    parser.add_argument("T1_path",       help = 'path to T1w with filename')
    parser.add_argument("gdc_file",      help = 'path to GDC with filename')
    parser.add_argument("fsl_dir",       help = 'path to FSL')
    parser.add_argument("fnirt_data_dir",    help = 'path to fnirt folder')
    parser.add_argument("templ",         help = 'path to templates folder')
    parser.add_argument("idp_data",      help = 'path to IDP ref folder')

    args            = parser.parse_args()   
    T1_path         = args.T1_path    
    gdc_file        = args.gdc_file
    FSLDIR          = args.fsl_dir
    fnirt_data_dir  = args.fnirt_data_dir
    templ           = args.templ
    IDP_data        = args.idp_data
    
    print('\n ---- Running GDC : %s ---- \n'% os.path.basename(T1_path))
    T1_gdc(T1_path,gdc_file,FSLDIR,fnirt_data_dir,templ)
    print('\n **** Completed GDC : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running T1 brain extraction : %s ---- \n'% os.path.basename(T1_path))
    T1_brain_extract(T1_path,gdc_file,FSLDIR,fnirt_data_dir,templ)
    print('\n **** Completed T1 brain extraction : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running T1 defacing : %s ---- \n'% os.path.basename(T1_path))
    T1_defacing(T1_path,FSLDIR,fnirt_data_dir,templ)
    print('\n **** Completed T1 defacing : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running T1 FAST : %s ---- \n'% os.path.basename(T1_path))
    T1_fast(T1_path,FSLDIR,fnirt_data_dir,templ)
    print('\n **** Completed T1 FAST : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running T1 FIRST : %s ---- \n'% os.path.basename(T1_path))
    T1_first(T1_path,FSLDIR,fnirt_data_dir,templ)
    print('\n **** Completed T1 FIRST : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running T1 SIENAX : %s ---- \n'% os.path.basename(T1_path))
    T1_sienax(T1_path,FSLDIR,fnirt_data_dir,templ)
    print('\n **** Completed T1 SIENAX : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running IDP calculation : %s ---- \n'% os.path.basename(T1_path))
    T1_idps(T1_path,IDP_data)
    print('\n **** Completed IDP calculation : %s **** \n'% os.path.basename(T1_path))
    print('\n ---- Running IDP json creation : %s ---- \n'% os.path.basename(T1_path))
    IDP_json_generation(T1_path,IDP_data)
    print('\n ---- Completed IDP json creation : %s ---- \n'% os.path.basename(T1_path))
    print('\n **** Finished! : %s **** \n'% os.path.basename(T1_path))
    print('\n **** Required IDPs are stored in %s **** \n'% op.join(op.dirname(T1_path),"IDPs/IDPs_Report.json"))
    
if __name__ == "__main__":
    main()