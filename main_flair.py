import os
import os.path as op
import glob 
import argparse
import json
from shutil import copyfile
from gradunwarp.core.gradient_unwarp_apply import gradient_unwarp_apply
from fsl import wrappers
from fsl.transform import affine, flirt

def T2_FLAIR_GDC(T2_FLAIR_path,gdc_file):
    
    T2_FLAIR_orig = T2_FLAIR_path
    os.chdir(os.path.dirname(T2_FLAIR_orig))
    gdc           = gdc_file

    T2_FLAIR_GDC          = op.join(op.dirname(T2_FLAIR_orig),'T2_FLAIR_GDC')
    T2_FLAIR_orig_ud      = "T2_FLAIR_orig_ud.nii.gz"
    T2_FLAIR_orig_ud_warp = "T2_FLAIR_orig_ud_warp.nii.gz"

    if gdc not in (None, "", "none"):
        # Calculate and apply the Gradient Distortion Unwarp
        # TODO: Review the "half=True" in next version
        gradient_unwarp_apply(WD=T2_FLAIR_GDC, infile=T2_FLAIR_orig,
                              outfile=T2_FLAIR_orig_ud,
                              owarp=T2_FLAIR_orig_ud_warp, gradcoeff=gdc,
                              vendor='siemens', nojac=True, half=True)
    else:
        copyfile(src=T2_FLAIR_orig, dst=T2_FLAIR_orig_ud)
        
def T2_FLAIR_brain_extract(T2_FLAIR_path, gdc_file, T1_dir, FSLDIR, templ):
    
    T2_FLAIR_orig = T2_FLAIR_path
    os.chdir(op.dirname(T2_FLAIR_orig))
    
    gdc = gdc_file

    tmp_dir= os.path.join(op.dirname(T2_FLAIR_orig),"tmp_dir")

    isExist = os.path.exists(tmp_dir)

    if not isExist:
        os.mkdir(tmp_dir)

    T2_FLAIR_tmp_1_mat = op.join(tmp_dir, 'T2_FLAIR_tmp_1.mat')
    T2_FLAIR_tmp_2_mat = op.join(tmp_dir, 'T2_FLAIR_tmp_2.mat')

    T2_FLAIR_orig_ud      = "T2_FLAIR_orig_ud.nii.gz"
    T2_FLAIR_orig_ud_warp = "T2_FLAIR_orig_ud_warp.nii.gz"

    # Take T2 to T1 and also the brain mask
    wrappers.flirt(src=T2_FLAIR_orig_ud, ref=op.join(T1_dir,"T1_orig_ud"),
                   omat=T2_FLAIR_tmp_1_mat, dof=6)
    wrappers.concatxfm(atob=T2_FLAIR_tmp_1_mat, btoc=op.join(T1_dir,"T1_orig_ud_to_T1.mat"),
                       atoc=T2_FLAIR_tmp_2_mat)

    # The T2_FLAIR.nii.gz in the output of this
    # flirt will be overwritten in case of GDC
    wrappers.flirt(src=T2_FLAIR_orig_ud, ref=op.join(T1_dir,"T1_brain"),
                   refweight=op.join(T1_dir,"T1_brain_mask"), nosearch=True,
                   init=T2_FLAIR_tmp_2_mat, out="T2_FLAIR",
                   omat="T2_FLAIR_orig_ud_to_T2_FLAIR.mat", dof=6)

    if gdc not in (None, "", "none"):
        wrappers.applywarp(src=T2_FLAIR_orig, ref=op.join(T1_dir,"T1_brain"), out="T2_FLAIR",
                           w=T2_FLAIR_orig_ud_warp,
                           postmat="T2_FLAIR_orig_ud_to_T2_FLAIR.mat",
                           rel=True, interp='spline')

    copyfile(src=op.join(T1_dir,"T1_brain_mask.nii.gz"), dst="T2_FLAIR_brain_mask.nii.gz")
    wrappers.fslmaths("T2_FLAIR").mul("T2_FLAIR_brain_mask").run("T2_FLAIR_brain")

    # enerate the linear matrix from T2 to MNI (Needed for defacing)
    wrappers.concatxfm(atob="T2_FLAIR_orig_ud_to_T2_FLAIR.mat",
                       btoc=T2_FLAIR_tmp_1_mat,
                       atoc="T2_FLAIR_orig_ud_to_MNI_linear.mat")
    copyfile(src=op.join(T1_dir,"T1_to_MNI_linear.mat"), dst="T2_FLAIR_to_MNI_linear.mat")

    # Generate the non-linearly warped T2 in MNI
    # (Needed for post-freesurfer processing)
    if gdc not in (None, "", "none"):
        wrappers.convertwarp(ref=op.join(FSLDIR,"data/standard/MNI152_T1_1mm"), warp1=T2_FLAIR_orig_ud_warp,
                             midmat="T2_FLAIR_orig_ud_to_T2_FLAIR.mat",
                             warp2=op.join(T1_dir,"T1_to_MNI_warp"),
                             out="T2_FLAIR_orig_to_MNI_warp")
    else:
        wrappers.convertwarp(ref=op.join(FSLDIR,"data/standard/MNI152_T1_1mm"),
                             premat="T2_FLAIR_orig_ud_to_T2_FLAIR.mat",
                             warp1=op.join(T1_dir,"T1_to_MNI_warp"),
                             out="T2_FLAIR_orig_to_MNI_warp")
    wrappers.applywarp(src=T2_FLAIR_orig, ref=op.join(FSLDIR,"data/standard/MNI152_T1_1mm"),
                       out="T2_FLAIR_brain_to_MNI",
                       w="T2_FLAIR_orig_to_MNI_warp",
                       rel=True, interp='spline')
    wrappers.fslmaths("T2_FLAIR_brain_to_MNI").mul(op.join(templ,"MNI152_T1_1mm_brain_mask.nii.gz")).run("T2_FLAIR_brain_to_MNI")

# T2_FLAIR_defacing: this section is working 
def T2_FLAIR_defacing(T2_FLAIR_path,FSLDIR,templ):
    
    T2_FLAIR_orig = T2_FLAIR_path
    os.chdir(op.dirname(T2_FLAIR_orig))

    tmp_dir= os.path.join(op.dirname(T2_FLAIR_orig),"tmp_dir")

    isExist = os.path.exists(tmp_dir)

    if not isExist:
        os.mkdir(tmp_dir)

    T2_FLAIR_tmp_1     = op.join(tmp_dir, 'T2_FLAIR_tmp_1.nii.gz') # Fidel's script '.' is missing
    T2_FLAIR_tmp_3_mat = op.join(tmp_dir, 'T2_FLAIR_tmp_3.mat')

    BigFoV_mask_mat = op.join(templ,'MNI_to_MNI_BigFoV_facemask.mat')
    BigFoV_mask     = op.join(templ,'MNI152_T1_1mm_BigFoV_facemask')

    # TODO: Replace this part with proper call to fsl_deface
    # Defacing T2_FLAIR_orig
    wrappers.concatxfm(atob="T2_FLAIR_orig_ud_to_T2_FLAIR.mat",
                       btoc="T2_FLAIR_to_MNI_linear.mat",
                       atoc=T2_FLAIR_tmp_3_mat)
    wrappers.concatxfm(atob=T2_FLAIR_tmp_3_mat,
                       btoc=BigFoV_mask_mat,
                       atoc=T2_FLAIR_tmp_3_mat)
    wrappers.invxfm(inmat=T2_FLAIR_tmp_3_mat, omat=T2_FLAIR_tmp_3_mat)
    wrappers.applyxfm(src=BigFoV_mask,
                      ref="T2_FLAIR_orig", mat=T2_FLAIR_tmp_3_mat,
                      out="T2_FLAIR_orig_defacing_mask",
                      interp="trilinear")
    wrappers.fslmaths("T2_FLAIR_orig_defacing_mask").binv().mul("T2_FLAIR_orig").run("T2_FLAIR_orig_defaced")

    # Defacing T2_FLAIR
    copyfile(src="T2_FLAIR.nii.gz", dst=T2_FLAIR_tmp_1)
    wrappers.concatxfm(atob="T2_FLAIR_to_MNI_linear.mat",
                       btoc=BigFoV_mask_mat,
                       atoc=T2_FLAIR_tmp_3_mat)
    wrappers.invxfm(inmat=T2_FLAIR_tmp_3_mat, omat=T2_FLAIR_tmp_3_mat)
    wrappers.applyxfm(src=BigFoV_mask,
                      ref="T2_FLAIR", mat=T2_FLAIR_tmp_3_mat,
                      out="T2_FLAIR_defacing_mask", interp="trilinear")
    wrappers.fslmaths("T2_FLAIR_defacing_mask").binv().mul("T2_FLAIR").run("T2_FLAIR")
    
# apply bfc : this section is working 
def T2_FLAIR_apply_bfc(T2_FLAIR_path,T1_dir):
    T2_FLAIR_orig = T2_FLAIR_path
    os.chdir(op.dirname(T2_FLAIR_orig))

    if op.isfile(op.join(T1_dir,"T1_fast/T1_brain_bias.nii.gz")):
        wrappers.fslmaths("T2_FLAIR").div(op.join(T1_dir,"T1_fast/T1_brain_bias.nii.gz")).run("T2_FLAIR_unbiased")
        wrappers.fslmaths("T2_FLAIR_brain").div(op.join(T1_dir,"T1_fast/T1_brain_bias.nii.gz")).run("T2_FLAIR_unbiased_brain")
    else:
        print("WARNING: There was no bias field estimation. " +
              "Bias field correction cannot be applied to T2.")
        
# bianca
def T2_FLAIR_bianca(T2_FLAIR_path,T1_dir,bianca_data):
    T2_FLAIR_orig = T2_FLAIR_path
    os.chdir(op.dirname(T2_FLAIR_orig))
    
    lesions= os.path.join(op.dirname(T2_FLAIR_orig),"lesions")
    isExist = os.path.exists(lesions)
    if not isExist:
        os.mkdir(lesions)

    bianca_class_data = op.join(bianca_data,'bianca_class_data')

    if op.exists(bianca_class_data):
        # Create bianca mask
        wrappers.make_bianca_mask(op.join(T1_dir,"T1_unbiased"), op.join(T1_dir,"T1_fast/T1_brain_pve_0"),op.join(T1_dir,"T1_to_MNI_warp_coef_inv"), keep_files=False)

        copyfile(src=op.join(T1_dir,"T1_unbiased_brain_mask.nii.gz"),
                 dst="T1_unbiased_brain_mask.nii.gz")
        copyfile(src=op.join(T1_dir,"T1_unbiased_bianca_mask.nii.gz"),
                 dst="T1_unbiased_bianca_mask.nii.gz")
        copyfile(src=op.join(T1_dir,"T1_unbiased_ventmask.nii.gz"),
                 dst="T1_unbiased_ventmask.nii.gz")

        # Create masterfile for bianca
        filenames = [op.join(T1_dir,"T1_unbiased_brain.nii.gz"), op.join(op.dirname(T2_FLAIR_orig),"T2_FLAIR_unbiased.nii.gz"),
                     op.join(T1_dir,"T1_to_MNI_linear.mat")]
        with open("bianca_conf_file.txt", 'w', encoding="utf-8") as f:
            for j in filenames:
                f.write(j+" ")

        # Run bianca
        bianca_class_data = op.join(bianca_data,'bianca_class_data')

        wrappers.bianca(singlefile="bianca_conf_file.txt",
                        querysubjectnum=1, brainmaskfeaturenum=1,
                        loadclassifierdata=bianca_class_data,
                        matfeaturenum=3, featuresubset="1,2",
                        o="bianca_mask.nii.gz")

        # Multiply the lesions mask (bianca_mask)
        # by the "unbiased_bianca_mask"
        wrappers.fslmaths("bianca_mask").\
            mul("T1_unbiased_bianca_mask").\
            thr(0.8).bin().run("bianca_final_mask")

        vals = wrappers.fslstats("bianca_final_mask").V.run()[0]
        with open("volume.txt", 'wt', encoding="utf-8") as f:
            f.write(str(vals) + "\n")

        # Calculate total WMH (volume of the final mask),
        # periventricular WMH & deep WMH - 10mm Criteria
        wrappers.bianca_perivent_deep(wmh_map="bianca_final_mask",
                                      vent_mask="T1_unbiased_ventmask",
                                      minclustersize=0,
                                      outputdir="lesions",
                                      do_stats=2)
        
def main():
    parser = argparse.ArgumentParser(description='--Run T2 FLAIR processing (Biobank style)--')
    parser.add_argument("T2_FLAIR_path", help = 'path to T1w with filename')
    parser.add_argument("gdc_file",      help = 'path to GDC with filename')
    parser.add_argument("T1_dir",        help = 'path to T1 dir')
    parser.add_argument("fsl_dir",       help = 'path to FSL')
    parser.add_argument("bianca_data",   help = 'path to bianca data')
    parser.add_argument("templ",         help = 'path to templates folder')
    
    args              = parser.parse_args()   
    T2_FLAIR_path     = args.T2_FLAIR_path
    T1_dir            = args.T1_dir
    gdc_file          = args.gdc_file
    FSLDIR            = args.fsl_dir
    bianca_data       = args.bianca_data
    templ             = args.templ
    
    print('\n ---- Running GDC : %s ---- \n'% os.path.basename(T2_FLAIR_path))
    T2_FLAIR_GDC(T2_FLAIR_path,gdc_file)
    print('\n **** Completed GDC : %s **** \n'% os.path.basename(T2_FLAIR_path))
    print('\n ---- Running T2 FLAIR brain extraction : %s ---- \n'% os.path.basename(T2_FLAIR_path))
    T2_FLAIR_brain_extract(T2_FLAIR_path, gdc_file, T1_dir, FSLDIR, templ)
    print('\n **** Completed T2 FLAIR brain extraction : %s **** \n'% os.path.basename(T2_FLAIR_path))
    print('\n ---- Running T2 FLAIR defacing : %s ---- \n'% os.path.basename(T2_FLAIR_path))
    T2_FLAIR_defacing(T2_FLAIR_path,FSLDIR,templ)
    print('\n **** Completed T1 defacing : %s **** \n'% os.path.basename(T2_FLAIR_path))
    print('\n ---- Running T2 FLAIR bias correction : %s ---- \n'% os.path.basename(T2_FLAIR_path))
    T2_FLAIR_apply_bfc(T2_FLAIR_path,T1_dir)
    print('\n **** Completed T2 FLAIR bias correction : %s **** \n'% os.path.basename(T2_FLAIR_path))
    print('\n ---- Running T2 FLAIR BIANCA : %s ---- \n'% os.path.basename(T2_FLAIR_path))
    T2_FLAIR_bianca(T2_FLAIR_path,T1_dir,bianca_data)
    print('\n **** Completed T2 FLAIR BIANCA : %s **** \n'% os.path.basename(T2_FLAIR_path))
    
if __name__ == "__main__":
    main()